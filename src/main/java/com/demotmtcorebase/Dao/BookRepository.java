package com.demotmtcorebase.Dao;

import com.demotmtcorebase.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<Book,Integer> , JpaSpecificationExecutor<Book> {
}
