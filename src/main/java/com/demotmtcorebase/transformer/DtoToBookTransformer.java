package com.demotmtcorebase.transformer;

import com.demotmtcorebase.Dto.BookDto;
import com.demotmtcorebase.entities.Book;
import com.tmt.core.base.SharedObjectToEntityTransformer;
import com.tmt.core.transformer.AbstractCopyPropertiesTransformer;

public class DtoToBookTransformer extends AbstractCopyPropertiesTransformer<BookDto, Book> implements SharedObjectToEntityTransformer<BookDto,Book,Integer> {
}
