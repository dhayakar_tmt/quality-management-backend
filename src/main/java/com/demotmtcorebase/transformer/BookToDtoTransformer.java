package com.demotmtcorebase.transformer;

import com.demotmtcorebase.Dto.BookDto;
import com.demotmtcorebase.entities.Book;
import com.tmt.core.base.EntityToSharedObjectTransformer;
import com.tmt.core.transformer.AbstractCopyPropertiesTransformer;

public class BookToDtoTransformer extends AbstractCopyPropertiesTransformer<Book, BookDto> implements EntityToSharedObjectTransformer<Book,BookDto,Integer> {
}
