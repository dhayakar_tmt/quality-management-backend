package com.demotmtcorebase.service;

import com.demotmtcorebase.Dao.BookRepository;
import com.demotmtcorebase.entities.Book;
import com.tmt.core.base.AbstractBaseEntityCommandService;
import org.springframework.stereotype.Service;

@Service
public class BookService extends AbstractBaseEntityCommandService<Book,Integer> {
    public BookService(final BookRepository bookRepository){
          super(bookRepository);
    }
}
