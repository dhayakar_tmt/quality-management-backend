package com.demotmtcorebase.Dto;

import com.tmt.core.base.BaseSharedObject;
import lombok.Data;

@Data
public class BookDto implements BaseSharedObject<Integer> {

    @Override
    public Long getLockVersion() {
        return null;
    }

    @Override
    public Integer getId() {
        return null;
    }
}
