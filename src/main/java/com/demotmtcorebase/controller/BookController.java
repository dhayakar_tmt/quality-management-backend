package com.demotmtcorebase.controller;

import com.demotmtcorebase.Dto.BookDto;
import com.demotmtcorebase.entities.Book;
import com.demotmtcorebase.service.BookService;
import com.tmt.core.base.BaseEntity;
import com.tmt.core.query.AbstractBaseSharedObjectQueryController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class BookController extends AbstractBaseSharedObjectQueryController<Book,Integer, BookDto> {

    @Autowired
    private BookService bookService;
}
